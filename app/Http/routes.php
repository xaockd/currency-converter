<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'currency.converter.index', 'uses' => 'Currency\ConverterController@index']);

Route::get('/exchange', ['as' => 'currency.converter.exchange', 'uses' => 'Currency\ConverterController@getExchange']);

Route::post('/api_exchange', ['as' => 'currency.converter.api.exchange', 'uses' => 'Currency\ConverterController@apiExchange']);

