<?php
/**
 * Created by PhpStorm.
 * User: xaoc
 * Date: 12/14/15
 * Time: 4:55 PM
 */

namespace App\Http\Requests;


use App\Services\Currency\Currency;
use Illuminate\Http\JsonResponse;

class ConverterRequest extends Request
{
    public function rules()
    {
        $currency_rules = 'required|string|size:3|in:'.implode(',', Currency::$list);

        return [
            'currency_from' => $currency_rules,
            'currency_to' => $currency_rules,
            'amount' => 'numeric|required|min:1'
        ];
    }

    public function response(array $errors)
    {
        if ($this->ajax() || $this->wantsJson()) {
            return new JsonResponse(['errors' => $errors], 422);
        }

        return $this->redirector->to($this->getRedirectUrl())
            ->withInput($this->except($this->dontFlash))
            ->withErrors($errors, $this->errorBag);
    }

    public function authorize()
    {
        return true;
    }
}