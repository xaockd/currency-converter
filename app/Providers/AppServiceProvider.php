<?php

namespace App\Providers;

use App\Services\Currency\ExchangeRates;
use App\Services\Currency\Transformer\EcbTransformer;
use App\Services\Currency\Transformer\UkraineBankTransformer;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ecb.exchange', function ($app) {
            $ratesResolver = new ExchangeRates(new EcbTransformer);
            $ratesResolver->setUri('www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml');

            return $ratesResolver;
        });

        $this->app->bind('ukraine.exchange', function ($app) {
            $ratesResolver = new ExchangeRates(new UkraineBankTransformer);
            $ratesResolver->setUri('http://bank-ua.com/export/currrate.xml');

            return $ratesResolver;
        });
    }
}
