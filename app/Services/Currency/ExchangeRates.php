<?php
/**
 * Created by PhpStorm.
 * User: xaoc
 * Date: 12/14/15
 * Time: 3:56 PM
 */

namespace App\Services\Currency;


use App\Services\Currency\Exceptions\ExchangeRatesException;
use App\Services\Currency\Transformer\TransformerContract;
use GuzzleHttp\Client as HttpClient;

class ExchangeRates
{
    /**
     * @var string
     */
    protected $uri = '';

    /**
     * @var TransformerContract
     */
    protected $transformer;

    /**
     * Rates constructor.
     * @param TransformerContract $transformer
     */
    public function __construct(TransformerContract $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * @return array|mixed
     * @throws ExchangeRatesException
     */
    public function get()
    {
        $client = new HttpClient;
        $request = $client->get($this->getUri()); //for better performance we can cache it

        if($request->getStatusCode() == 200) {
            $content = $request->getBody()->getContents();

            return $this->transformer->transform($content);
        }

        throw new ExchangeRatesException('Exchange rates service unavailable');
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return !$this->uri ? config('app.exchange_rate_provider', 'www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml') : $this->uri;
    }

    /**
     * @param mixed $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }


}