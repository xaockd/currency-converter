Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('[name="csrf_token"]').getAttribute('content');

var app = new Vue({
    el: '#app',
    data: {
        currency_from: 'EUR',
        currency_to: 'USD',
        amount: 1,
        response: {}
    },
    methods: {
        convert: function (event) {

            event.preventDefault();
            this.clearOldMessages();

            var url = '/api_exchange';

            var payload = {
                currency_from: this.currency_from,
                currency_to: this.currency_to,
                amount: this.amount,
            };

            this.$http.post(url, payload, function (data, status, request) {
                this.response = data;

            }).error(function (data, status, request) {
                if(data.error) {
                    data.errors = [data.error]; //convert exception error to array
                }
                this.response = data;
            });

        },
        clearOldMessages: function() {
            var messages = document.querySelector('.backend-message');
            if(messages) {
                messages.remove(); //remove backend messages
            }
            this.response = {};
        }
    }
});