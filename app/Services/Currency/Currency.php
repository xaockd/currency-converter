<?php
/**
 * Created by PhpStorm.
 * User: xaoc
 * Date: 12/14/15
 * Time: 3:13 PM
 */

namespace App\Services\Currency;


use App\Services\Currency\Exceptions\CurrencyNotFound;

class Currency
{
    /**
     * @var string
     */

    protected $iso_code;

    /**
     * @var float
     */
    protected $value;

    /**
     * @var array
     */
    public static $list = [
            'USD' => 'USD',
            'JPY' => 'JPY',
            'BGN' => 'BGN',
            'UAH' => 'UAH',
            'DKK' => 'DKK',
            'EUR' => 'EUR',
            'CZK' => 'CZK',
            'GBP' => 'GBP',
            'PLN' => 'PLN',
            'THB' => 'THB',
            'ZAR' => 'ZAR',
            'KRW' => 'KRW',
            'NOK' => 'NOK',
            'AUD' => 'AUD'
        ];

    /**
     * Currency constructor.
     * @param string $iso
     * @param float $value
     * @throws CurrencyNotFound
     */
    public function __construct($iso, $value)
    {
        if(!array_key_exists($iso, static::$list)) {
            throw new CurrencyNotFound("Currency with iso $iso not found");
        }

        $this->setIsoCode($iso);
        $this->setValue($value);
    }

    /**
     * @return mixed
     */
    public function getIsoCode()
    {
        return $this->iso_code;
    }

    /**
     * @param mixed $iso_code
     */
    public function setIsoCode($iso_code)
    {
        $this->iso_code = $iso_code;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

}