<?php
/**
 * Created by PhpStorm.
 * User: xaoc
 * Date: 12/14/15
 * Time: 3:26 PM
 */

namespace App\Http\Controllers\Currency;


use App\Http\Requests\Request;
use App\Services\Currency\Exceptions\CurrencyNotFound;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConverterRequest;
use App\Services\Currency\Converter;
use App\Services\Currency\Currency;
use App\Services\Currency\Exceptions\BaseException as ExchangeException;

class ConverterController extends Controller
{
    public function index()
    {
        $currencies = Currency::$list;

        return view('currency.index', compact('currencies'));
    }

    public function getExchange(ConverterRequest $request)
    {
        $result = $this->processExchange($request);

        session()->flash('result', $result);

        return redirect('/')->withInput($request->all());
    }

    public function apiExchange(ConverterRequest $request)
    {
        $result = $this->processExchange($request);

        $status = array_key_exists('error', $result) ? 422 : 200;

        return response()->json($result, $status);
    }

    protected function processExchange(Request $request)
    {
        $ratesResolver = app('ecb.exchange'); //you can also switch to app('ukraine.exchange')

        $rates = $ratesResolver->get();

        $result = [];

        try {
            $currency_from = $this->getCurrency($request->get('currency_from'), $rates);
            $currency_to   = $this->getCurrency($request->get('currency_to'), $rates);
            $amount        = $request->get('amount');
            $converter     = new Converter;

            $rate = $converter->exchange($currency_from, $currency_to, $amount);

            $result['text']          = sprintf('%.2F %s equals to %.4F %s', $amount, $currency_from->getIsoCode(), $rate, $currency_to->getIsoCode());
            $result['rate']          = $rate;
            $result['currency_from'] = $currency_from->getIsoCode();
            $result['currency_to']   = $currency_to->getIsoCode();
            $result['amount']        = $amount;
            $result['success']       = true;
        } catch (ExchangeException $e) {
            $result['error'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $currency
     * @param $rates
     * @return Currency
     * @throws CurrencyNotFound
     */
    private function getCurrency($currency, $rates)
    {
        if (array_key_exists($currency, $rates)) {
            return new Currency($currency, $rates[$currency]);
        }

        throw new CurrencyNotFound("Currency $currency not found in exchange rates list");
    }

}