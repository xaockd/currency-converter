@extends('layout.base')

@section('content')
    <div id="app">
        <h1>Currency Converter</h1>

        {!! Form::open(['method' => 'GET', 'route' => 'currency.converter.exchange']) !!}

        <div class="form-group {!! $errors->has('currency_from') ? 'has-error' : '' !!}">
            {!! Form::label('currency_from', 'Currency from') !!}
            {!! Form::select('currency_from', $currencies, null, ['class' => 'form-control', 'v-model' => 'currency_from']) !!}
            {!! $errors->first('currency_from', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group {!! $errors->has('currency_to') ? 'has-error' : '' !!}">
            {!! Form::label('currency_to', 'Currency to') !!}
            {!! Form::select('currency_to', $currencies, null, ['class' => 'form-control', 'v-model' => 'currency_to']) !!}
            {!! $errors->first('currency_to', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group {!! $errors->has('amount') ? 'has-error' : '' !!}">
            {!! Form::label('amount', 'Amount') !!}
            {!! Form::number('amount', null, ['min' => 1, 'step' => '0.01', 'class' => 'form-control', 'v-model' => 'amount']) !!}
            {!! $errors->first('amount', '<p class="help-block">:message</p>') !!}
        </div>

        {!! Form::submit('Convert', ['class' => 'btn btn-default']) !!}

        {!! Form::button('AJAX Convert', ['class' => 'btn btn-default', 'v-on:click' => 'convert']) !!}

        {!! Form::close() !!}

        <div style="margin-top: 20px;">
            @if(session()->has('result.success'))
                <div class="alert alert-success backend-message" role="alert">
                    <span class="glyphicon glyphicon-glyphicon-ok" aria-hidden="true"></span>
                    {{ session()->get('result.text') }}
                </div>
            @endif

            @if(session()->has('result.error'))
                <div class="alert alert-dange backend-messager" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    {{ session()->get('result.error') }}
                </div>
            @endif

            <!-- Ajax errors-->
            <div class="alert alert-success" role="alert" v-if="response.success">
                <span class="glyphicon glyphicon-glyphicon-ok" aria-hidden="true"></span>
                    @{{ response.text }}
            </div>
            <div class="alert alert-danger" role="alert" v-if="response.errors">
                <span aria-hidden="true"></span>
                <ul v-if="response.errors">
                    <li v-for="error in response.errors">
                        @{{ error }}
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection()

@section('script')
    <script src="/js/currency_exchange.js"></script>
@append