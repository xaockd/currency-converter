<?php
/**
 * Created by PhpStorm.
 * User: xaoc
 * Date: 12/14/15
 * Time: 3:10 PM
 */

namespace App\Services\Currency;


class Converter implements ConverterContract
{
    /**
     * @param Currency $from
     * @param Currency $to
     * @param int $amount
     * @return string
     */
    public function exchange(Currency $from, Currency $to, $amount = 1)
    {
        if($from->getIsoCode() === $to->getIsoCode()) {
            $rate = 1;
        } else {
            $rateFrom = bcdiv(1, $from->getValue(), 4);

            $rate = bcmul($to->getValue(), $rateFrom, 4);
        }

        return bcmul($rate, $amount, 4);
    }
}