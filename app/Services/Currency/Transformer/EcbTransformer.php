<?php
/**
 * Created by PhpStorm.
 * User: xaoc
 * Date: 12/14/15
 * Time: 4:43 PM
 */

namespace App\Services\Currency\Transformer;


class EcbTransformer implements TransformerContract
{
    public function transform($data)
    {
        $xml = new \SimpleXMLElement($data);

        $result['EUR'] = 1; //Base currency for ECB

        foreach($xml->Cube->Cube->Cube as $exchange) {
            $currency = (string) $exchange->attributes()->currency;
            $value    = (string) $exchange->attributes()->rate;

            $result[$currency] = $value;
        }

        return $result;
    }
}