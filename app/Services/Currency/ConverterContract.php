<?php
/**
 * Created by PhpStorm.
 * User: xaoc
 * Date: 12/14/15
 * Time: 3:12 PM
 */

namespace App\Services\Currency;


interface ConverterContract
{
    public function exchange(Currency $from, Currency $to, $amount = 1);
}