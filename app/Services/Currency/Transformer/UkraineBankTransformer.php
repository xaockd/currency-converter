<?php
/**
 * Created by PhpStorm.
 * User: xaoc
 * Date: 12/14/15
 * Time: 6:19 PM
 */

namespace App\Services\Currency\Transformer;


class UkraineBankTransformer implements TransformerContract
{
    /**
     * @param $data
     * @return mixed
     */
    public function transform($data)
    {
        $xml = new \SimpleXMLElement($data);

        $result['UAH'] = 1; //Base currency for Ukraine bank

        foreach($xml as $exchange) {
            $currency = (string) $exchange->char3;
            $value    = bcdiv(1, bcdiv($exchange->rate, $exchange->size, 4), 4);

            $result[$currency] = $value;
        }

        return $result;
    }
}