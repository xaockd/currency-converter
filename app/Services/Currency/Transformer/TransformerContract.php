<?php
/**
 * Created by PhpStorm.
 * User: xaoc
 * Date: 12/14/15
 * Time: 4:42 PM
 */

namespace App\Services\Currency\Transformer;


interface TransformerContract
{
    /**
     * @param $data
     * @return mixed
     */
    public function transform($data);
}